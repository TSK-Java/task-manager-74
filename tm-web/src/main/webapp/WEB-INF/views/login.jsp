<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Авторизация</title>
    </head>
    <body onload='document.f.username.focus();'>
        <form name='f' action='/auth' method='POST'>
            <table width="400" border="1" cellpadding="10" cellspacing="0" align="center" >
                <tr>
                    <td colspan="2" align="center" >
                        <h3>Авторизация</h3>
                    </td>
                </tr>
                <tr>
                    <td>Логин:</td>
                    <td><input type='text' name='username' value=''></td>
                </tr>
                <tr>
                    <td>Пароль:</td>
                    <td><input type='password' name='password'/></td>
                </tr>
                <tr>
                    <td colspan='2'><input name="submit" type="submit" value="Вход"/></td>
                </tr>
            </table>
        </form>
    </body>
</html>