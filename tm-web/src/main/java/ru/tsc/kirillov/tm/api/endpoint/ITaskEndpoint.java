package ru.tsc.kirillov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kirillov.tm.dto.model.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/task")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<TaskDto> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    TaskDto save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody List<TaskDto> tasks
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

}
