package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    @Transactional
    ProjectDto create(@Nullable String userId);

    @NotNull
    @Transactional
    ProjectDto create(@Nullable String userId, @Nullable String name);

    @NotNull
    @Transactional
    ProjectDto save(@Nullable String userId, @Nullable ProjectDto project);

    @Nullable
    Collection<ProjectDto> findAll(@Nullable String userId);

    @Nullable
    ProjectDto findById(@Nullable String userId, @Nullable String id);

    @Transactional
    void removeById(@Nullable String userId, @Nullable String id);

    @Transactional
    void remove(@Nullable String userId, @Nullable ProjectDto project);

    @Transactional
    void remove(@Nullable String userId, @Nullable List<ProjectDto> projects);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Transactional
    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
