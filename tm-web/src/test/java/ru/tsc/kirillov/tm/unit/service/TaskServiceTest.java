package ru.tsc.kirillov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.config.DatabaseConfiguration;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.marker.UnitCategory;
import ru.tsc.kirillov.tm.util.UserUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class TaskServiceTest {

    @NotNull
    @Autowired
    private ITaskService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final TaskDto task = new TaskDto("id", "Name");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(UserUtil.getUserId(), task);
    }

    @After
    public void clean() {
        service.clear(UserUtil.getUserId());
    }

    @Test
    public void create() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.create(userId);
        Assert.assertEquals(2, service.count(userId));
    }

    @Test
    public void createName() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final TaskDto taskCreate = service.create(userId, name);
        Assert.assertEquals(2, service.count(userId));
        @Nullable final TaskDto taskFind = service.findById(userId, taskCreate.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskCreate.getId(), taskFind.getId());
        Assert.assertEquals(name, taskCreate.getName());
        Assert.assertEquals(name, taskFind.getName());
    }

    @Test
    public void save() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @NotNull final TaskDto taskSave = new TaskDto(userId, "Name");
        service.save(userId, taskSave);
        Assert.assertEquals(2, service.count(userId));
        @Nullable final TaskDto taskFind = service.findById(userId, taskSave.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskSave.getId(), taskFind.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @Nullable final Collection<TaskDto> taskList = service.findAll(userId);
        Assert.assertNotNull(taskList);
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @Nullable final TaskDto taskFind = service.findById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskFind.getId(), task.getId());
    }

    @Test
    public void removeById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.removeById(userId, task.getId());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void remove() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.remove(userId, task);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeList() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.remove(userId, Collections.singletonList(task));
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void existsById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertTrue(service.existsById(userId, task.getId()));
        Assert.assertFalse(service.existsById(userId, UUID.randomUUID().toString()));
    }

    @Test
    public void clear() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.clear(userId);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void count() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
    }

}
