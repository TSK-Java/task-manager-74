package ru.tsc.kirillov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.kirillov.tm.client.AuthSoapEndpointClient;
import ru.tsc.kirillov.tm.client.ProjectSoapEndpointClient;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final ProjectDto project = new ProjectDto("id", "Name");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull Map<String, List<String>> headers =
                CastUtils.cast(
                        (Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS)
                );
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        projectEndpoint.save(project);
    }

    @After
    @SneakyThrows
    public void clean() {
        projectEndpoint.clear();
    }

    @Test
    public void findAll() {
        @Nullable final List<ProjectDto> projects = projectEndpoint.findAll();
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
    }

    @Test
    public void findById() {
        @Nullable final ProjectDto projectFind = projectEndpoint.findById(project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(projectFind.getId(), project.getId());
    }

    @Test
    public void existsById() {
        boolean result = projectEndpoint.existsById(project.getId());
        Assert.assertTrue(result);
        result = projectEndpoint.existsById(UUID.randomUUID().toString());
        Assert.assertFalse(result);
    }

    @Test
    public void save() {
        @NotNull final ProjectDto projectSave = new ProjectDto("id", "Name");
        @NotNull final ProjectDto projectResult = projectEndpoint.save(projectSave);
        Assert.assertNotNull(projectResult);
        Assert.assertEquals(projectResult.getId(), projectSave.getId());
    }

    @Test
    public void delete() {
        Assert.assertEquals(1, projectEndpoint.count());
        projectEndpoint.delete(project);
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void deleteAll() {
        Assert.assertEquals(1, projectEndpoint.count());
        projectEndpoint.deleteAll(Collections.singletonList(project));
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void clear() {
        Assert.assertEquals(1, projectEndpoint.count());
        projectEndpoint.clear();
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void deleteById() {
        Assert.assertEquals(1, projectEndpoint.count());
        projectEndpoint.deleteById(project.getId());
        Assert.assertEquals(0, projectEndpoint.count());
    }

    @Test
    public void count() {
        Assert.assertEquals(1, projectEndpoint.count());
    }

}
