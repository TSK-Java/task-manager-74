package ru.tsc.kirillov.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.kirillov.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.kirillov.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.kirillov.tm.client.AuthSoapEndpointClient;
import ru.tsc.kirillov.tm.client.TaskSoapEndpointClient;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

@Category(IntegrationCategory.class)
public class TaskSoapEndpointTest {

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final TaskDto task = new TaskDto("id", "Name");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("test", "test").getSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull Map<String, List<String>> headers =
                CastUtils.cast(
                        (Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS)
                );
        if (headers == null) headers = new HashMap<>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        taskEndpoint.save(task);
    }

    @After
    @SneakyThrows
    public void clean() {
        taskEndpoint.clear();
    }

    @Test
    public void findAll() {
        @Nullable final List<TaskDto> tasks = taskEndpoint.findAll();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findById() {
        @Nullable final TaskDto taskFind = taskEndpoint.findById(task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(taskFind.getId(), task.getId());
    }

    @Test
    public void existsById() {
        boolean result = taskEndpoint.existsById(task.getId());
        Assert.assertTrue(result);
        result = taskEndpoint.existsById(UUID.randomUUID().toString());
        Assert.assertFalse(result);
    }

    @Test
    public void save() {
        @NotNull final TaskDto taskSave = new TaskDto("id", "Name");
        @NotNull final TaskDto taskResult = taskEndpoint.save(taskSave);
        Assert.assertNotNull(taskResult);
        Assert.assertEquals(taskResult.getId(), taskSave.getId());
    }

    @Test
    public void delete() {
        Assert.assertEquals(1, taskEndpoint.count());
        taskEndpoint.delete(task);
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void deleteAll() {
        Assert.assertEquals(1, taskEndpoint.count());
        taskEndpoint.deleteAll(Collections.singletonList(task));
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void clear() {
        Assert.assertEquals(1, taskEndpoint.count());
        taskEndpoint.clear();
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void deleteById() {
        Assert.assertEquals(1, taskEndpoint.count());
        taskEndpoint.deleteById(task.getId());
        Assert.assertEquals(0, taskEndpoint.count());
    }

    @Test
    public void count() {
        Assert.assertEquals(1, taskEndpoint.count());
    }

}
