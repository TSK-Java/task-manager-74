package ru.tsc.kirillov.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.marker.UnitCategory;
import ru.tsc.kirillov.tm.util.UserUtil;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;

@SpringBootTest
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private IProjectService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final ProjectDto project = new ProjectDto("id", "Name");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        service.save(UserUtil.getUserId(), project);
    }

    @After
    public void clean() {
        service.clear(UserUtil.getUserId());
    }

    @Test
    public void create() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.create(userId);
        Assert.assertEquals(2, service.count(userId));
    }

    @Test
    public void createName() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final ProjectDto projectCreate = service.create(userId, name);
        Assert.assertEquals(2, service.count(userId));
        @Nullable final ProjectDto projectFind = service.findById(userId, projectCreate.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(projectCreate.getId(), projectFind.getId());
        Assert.assertEquals(name, projectCreate.getName());
        Assert.assertEquals(name, projectFind.getName());
    }

    @Test
    public void save() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @NotNull final ProjectDto projectSave = new ProjectDto(userId, "Name");
        service.save(userId, projectSave);
        Assert.assertEquals(2, service.count(userId));
        @Nullable final ProjectDto projectFind = service.findById(userId, projectSave.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(projectSave.getId(), projectFind.getId());
    }

    @Test
    public void findAll() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @Nullable final Collection<ProjectDto> projectList = service.findAll(userId);
        Assert.assertNotNull(projectList);
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void findById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        @Nullable final ProjectDto projectFind = service.findById(userId, project.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(projectFind.getId(), project.getId());
    }

    @Test
    public void removeById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.removeById(userId, project.getId());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void remove() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.remove(userId, project);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeList() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.remove(userId, Collections.singletonList(project));
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void existsById() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertTrue(service.existsById(userId, project.getId()));
        Assert.assertFalse(service.existsById(userId, UUID.randomUUID().toString()));
    }

    @Test
    public void clear() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
        service.clear(userId);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void count() {
        @NotNull final String userId = UserUtil.getUserId();
        Assert.assertEquals(1, service.count(userId));
    }

}
