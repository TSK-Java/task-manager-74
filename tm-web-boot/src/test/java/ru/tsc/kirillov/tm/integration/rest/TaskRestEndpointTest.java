package ru.tsc.kirillov.tm.integration.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.tsc.kirillov.tm.dto.model.Result;
import ru.tsc.kirillov.tm.dto.model.TaskDto;
import ru.tsc.kirillov.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @Nullable
    private static String sessionId;

    @NotNull
    private static final String TASK_URL = "http://localhost:8080/api/task/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @NotNull
    private final TaskDto task = new TaskDto("id", "Name");

    @BeforeClass
    public static void beforeClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?login=test&password=test";
        header.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<Result> response =
                restTemplate.postForEntity(url, new HttpEntity<>(""), Result.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        @NotNull final List<HttpCookie> cookies = HttpCookie.parse(
                headersResponse.getFirst(HttpHeaders.SET_COOKIE)
        );
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName()))
                .findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
    }

    private static <T> ResponseEntity<T> sendRequest(
            @NotNull final String url,
            @NotNull final HttpMethod method,
            @NotNull final HttpEntity httpEntity,
            @NotNull final Class<T> responseType
    ) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, responseType);
    }

    @Before
    public void initTest() {
        @NotNull final String url = TASK_URL + "save/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(task, header), TaskDto.class);
    }

    @After
    public void clean() {
        @NotNull final String url = TASK_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), TaskDto.class);
    }

    @AfterClass
    public static void logout() {
        @NotNull final String logoutUrl = "http://localhost:8080/api/auth/logout";
        sendRequest(logoutUrl, HttpMethod.POST, new HttpEntity<>(header), Result.class);
    }

    private long count() {
        @NotNull final String logoutUrl = TASK_URL + "count";
        @NotNull final ResponseEntity<Long> response =
                sendRequest(logoutUrl, HttpMethod.GET, new HttpEntity<>(header), Long.class);
        if (response.getStatusCode() != HttpStatus.OK) return 0;
        @Nullable Long result = response.getBody();
        if (result == null) return 0;
        return result;
    }

    @Test
    public void findAllTest() {
        @NotNull final String url = TASK_URL + "findAll/";
        @NotNull final ResponseEntity<List> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), List.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final List taskList = response.getBody();
        Assert.assertNotNull(taskList);
        Assert.assertEquals(1, taskList.size());
    }

    @Test
    public void findById() {
        @NotNull final String url = TASK_URL + "findById/" + task.getId();
        @NotNull final ResponseEntity<TaskDto> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), TaskDto.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final TaskDto taskFind = response.getBody();
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    public void existsById() {
        @NotNull String url = TASK_URL + "existsById/" + task.getId();
        @NotNull ResponseEntity<Boolean> response =
                sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable Boolean result = response.getBody();
        Assert.assertNotNull(result);
        Assert.assertTrue(result);

        url = TASK_URL + "existsById/" + UUID.randomUUID();
        response = sendRequest(url, HttpMethod.GET, new HttpEntity<>(header), Boolean.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        result = response.getBody();
        Assert.assertNotNull(result);
        Assert.assertFalse(result);
    }

    @Test
    public void save() {
        @NotNull final String url = TASK_URL + "save/";
        @NotNull final TaskDto taskSave = new TaskDto("id", "Name");
        @NotNull final ResponseEntity<TaskDto> response =
                sendRequest(url, HttpMethod.POST, new HttpEntity<>(taskSave, header), TaskDto.class);
        Assert.assertEquals(response.getStatusCode(), HttpStatus.OK);
        @Nullable final TaskDto taskResult = response.getBody();
        Assert.assertNotNull(taskResult);
        Assert.assertEquals(taskResult.getId(), taskSave.getId());
    }

    @Test
    public void delete() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "delete/";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(task, header), TaskDto.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteAll() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "deleteAll/";
        sendRequest(
                url,
                HttpMethod.DELETE,
                new HttpEntity<>(Collections.singletonList(task), header),
                TaskDto.class
        );
        Assert.assertEquals(0, count());
    }

    @Test
    public void clear() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "clear";
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), TaskDto.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void deleteById() {
        Assert.assertEquals(1, count());
        @NotNull final String url = TASK_URL + "deleteById/" + task.getId();
        sendRequest(url, HttpMethod.DELETE, new HttpEntity<>(header), TaskDto.class);
        Assert.assertEquals(0, count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, count());
    }

}
