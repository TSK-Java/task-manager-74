package ru.tsc.kirillov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@NoArgsConstructor
public final class Result {

    @NotNull
    private Boolean success = true;

    @NotNull
    private String message = "";

    public Result(@NotNull final Boolean success) {
        this.success = success;
    }

    public Result(@NotNull final Exception e) {
        success = false;
        message = String.format("Исключение '%s' с сообщением: %s", e.getClass(), e.getMessage());
    }

}
