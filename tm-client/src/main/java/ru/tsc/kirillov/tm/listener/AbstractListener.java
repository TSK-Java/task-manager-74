package ru.tsc.kirillov.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.api.service.ITokenService;
import ru.tsc.kirillov.tm.enumerated.Role;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements ICommand {

    @NotNull
    @Autowired
    protected ITokenService tokenService;

    @Nullable
    public abstract String getName();

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public abstract String getDescription();

    public abstract void handler(@NotNull final ConsoleEvent event);

    @Nullable
    public abstract Role[] getRoles();

    @Nullable
    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    @Override
    public String toString() {
        @NotNull String result = "";
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();

        if (name != null && !name.isEmpty())
            result += name;
        if (argument != null && !argument.isEmpty())
            result += ", " + argument;
        if (description != null && !description.isEmpty())
            result += " - " + description;

        if (!result.isEmpty())
            return result;
        else
            return super.toString();
    }

}
