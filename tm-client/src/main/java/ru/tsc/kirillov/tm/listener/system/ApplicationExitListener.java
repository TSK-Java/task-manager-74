package ru.tsc.kirillov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kirillov.tm.event.ConsoleEvent;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Закрытие приложения.";
    }

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }

}
