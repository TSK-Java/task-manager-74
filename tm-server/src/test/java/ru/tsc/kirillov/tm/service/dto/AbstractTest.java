package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.kirillov.tm.api.service.dto.IUserDtoService;
import ru.tsc.kirillov.tm.configuration.ServerConfiguration;

public abstract class AbstractTest {

    @NotNull
    protected static AnnotationConfigApplicationContext CONTEXT;

    @NotNull
    protected IUserDtoService userService;

    @BeforeClass
    public static void initConnectionService() {
        CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);
    }

    @Before
    public void initialization() {
        userService = CONTEXT.getBean(IUserDtoService.class);
    }

    @After
    public void finalization() {
        userService.clear();
    }

}
