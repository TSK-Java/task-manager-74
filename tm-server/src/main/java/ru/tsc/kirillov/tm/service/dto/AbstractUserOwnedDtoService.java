package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.tsc.kirillov.tm.api.service.dto.IUserOwnedDtoService;
import ru.tsc.kirillov.tm.dto.model.AbstractUserOwnedDtoModel;
import ru.tsc.kirillov.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kirillov.tm.exception.field.IdEmptyException;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;
import ru.tsc.kirillov.tm.exception.system.IndexOutOfBoundsException;

import java.util.List;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedDtoModel, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R>
        implements IUserOwnedDtoService<M> {
    
    @NotNull
    @Override
    protected abstract IUserOwnedDtoRepository<M> getRepository();
    
    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        return repository.findAllByUserId(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        return getResult(repository.findAllByUserId(userId, getPageableForIndex(index)));
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return null;
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @Nullable final M result = repository.findFirstByUserIdAndId(userId, model.getId());
        if (result == null) return null;
        repository.delete(result);
        return result;
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @Nullable final M result = repository.findFirstByUserIdAndId(userId, id);
        if (result == null) throw new EntityNotFoundException();
        repository.deleteByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexOutOfBoundsException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        repository.delete(model);
        return model;
    }

    @Nullable
    @Override
    public M update(@Nullable final M model) {
        if (model == null) return null;
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        return repository.save(model);
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IUserOwnedDtoRepository<M> repository = getRepository();
        return repository.countByUserId(userId);
    }

}
