package ru.tsc.kirillov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kirillov.tm.api.repository.dto.IProjectDtoRepository;
import ru.tsc.kirillov.tm.api.service.dto.IProjectDtoService;
import ru.tsc.kirillov.tm.dto.model.ProjectDto;
import ru.tsc.kirillov.tm.exception.field.UserIdEmptyException;

@Service
public final class ProjectDtoService
        extends AbstractWbsDtoService<ProjectDto, IProjectDtoRepository>
        implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    protected IProjectDtoRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    public String[] findAllId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAllId(userId).toArray(new String[0]);
    }

}
