package ru.tsc.kirillov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.SessionDto;
import ru.tsc.kirillov.tm.dto.model.UserDto;

public interface IAuthService {

    @NotNull
    String login (@Nullable String login, @Nullable String password);

    @NotNull
    SessionDto validateToken(@Nullable String token);

    @NotNull
    UserDto registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
